#include "iceoryx_posh/popo/publisher.hpp"
#include "serviceData/drive_t.hpp"

#include <cstdint>

namespace serviceData
{
class Driver
{
  public:
    Driver();

    void
    TurnLeft() noexcept;
    void
    TurnRight() noexcept;
    void
    Stop() noexcept;
    void
    Forward() noexcept;
    void
    Backward() noexcept;

  private:
    void
    PublishDrivingCommand( const int8_t left, const int8_t right ) noexcept;

  private:
    iox::popo::Publisher< serviceData::drive_t > publisher;
    int8_t                                       speed = 40;
    int8_t speedReverse = static_cast< int8_t >( -speed );
    int8_t lastLeft{ 0 };
    int8_t lastRight{ 0 };
};
} // namespace serviceData


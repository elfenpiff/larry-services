#pragma once

namespace serviceData
{
template < typename T >
struct vector_t
{
    T x;
    T y;
    T z;
};
} // namespace serviceData

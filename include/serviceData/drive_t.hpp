#pragma once

#include <cstdint>

namespace serviceData
{
struct drive_t
{
    int8_t left{ 0 };  // -100 <= left  <= 100
    int8_t right{ 0 }; // -100 <= right <= 100
};
} // namespace serviceData

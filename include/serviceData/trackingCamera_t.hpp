#pragma once

#include "buildingBlocks/float_t.hpp"
#include "serviceData/buildingBlocks.hpp"
#include "units/Acceleration.hpp"
#include "units/AngularAcceleration.hpp"
#include "units/AngularVelocity.hpp"
#include "units/Confidence.hpp"
#include "units/Length.hpp"
#include "units/Velocity.hpp"

namespace serviceData
{
struct quaternion_t
{
    el3D::bb::float_t< 32 > x{ 0.0f };
    el3D::bb::float_t< 32 > y{ 0.0f };
    el3D::bb::float_t< 32 > z{ 0.0f };
    el3D::bb::float_t< 32 > w{ 0.0f };
};

struct trackingCamera_t
{
    vector_t< el3D::units::Acceleration >        acceleration;
    vector_t< el3D::units::AngularAcceleration > angularAcceleration;
    vector_t< el3D::units::Velocity >            velocity;
    vector_t< el3D::units::AngularVelocity >     angularVelocity;
    vector_t< el3D::units::Length >              translation;
    quaternion_t                                 rotation;
    el3D::units::Confidence                      trackerConfidence;
    el3D::units::Confidence                      mapperConfidence;
};
} // namespace serviceData

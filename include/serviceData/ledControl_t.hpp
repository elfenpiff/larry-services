#pragma once

namespace serviceData
{
struct ledControl_t
{
    bool red{ false };
    bool green{ false };
    bool blue{ false };
};
} // namespace serviceData

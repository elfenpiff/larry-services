#pragma once

#include "buildingBlocks/float_t.hpp"
#include "iceoryx_hoofs/cxx/string.hpp"
#include "iceoryx_hoofs/cxx/vector.hpp"

namespace serviceData
{
constexpr uint64_t STRING_SIZE             = 128;
constexpr uint64_t MAX_CPU_CORES           = 32;
constexpr uint64_t MAX_DNS_SERVER          = 4;
constexpr uint64_t MAX_NET_INTERFACES      = 4;
constexpr uint64_t MAX_NET_CONNECTIONS     = 32;
constexpr uint64_t MAX_THREADS_PER_PROCESS = 8;
constexpr uint64_t MAX_PROCESSES           = 128;

using string = iox::cxx::string< STRING_SIZE >;

namespace internal
{
struct cpuCore_t
{
    el3D::bb::float_t< 64 > frequencyInMhz{ 0.0 };
    el3D::bb::float_t< 64 > load{ 0.0 };
};
struct cpu_t
{
    string                                       model;
    uint64_t                                     numberOfCores{ 0u };
    el3D::bb::float_t< 64 >                      load{ 0.0 };
    iox::cxx::vector< cpuCore_t, MAX_CPU_CORES > cores;
};
struct memory_t
{
    uint64_t total{ 0u };
    uint64_t used{ 0u };
    uint64_t cached{ 0u };
    uint64_t buffer{ 0u };
    uint64_t swapTotal{ 0u };
    uint64_t swapUsed{ 0u };
};
struct netStat_t
{
    uint64_t                bytes{ 0u };
    uint64_t                packets{ 0u };
    el3D::bb::float_t< 64 > byteLoad{ 0.0 };
    el3D::bb::float_t< 64 > packetLoad{ 0.0 };
    uint64_t                errors{ 0u };
};
struct networkInterface_t
{
    netStat_t incoming;
    netStat_t outgoing;
    string    interfaceName;
    string    macAddress;
    string    ipAddress;
    string    broadCastAddress;
    string    networkMask;
};
struct connectionPart_t
{
    string   ipAddress;
    uint16_t port{ 0u };
};
struct connection_t
{
    connectionPart_t source;
    connectionPart_t destination;
};
struct network_t
{
    iox::cxx::vector< string, MAX_DNS_SERVER >                 dns;
    iox::cxx::vector< networkInterface_t, MAX_NET_INTERFACES > interfaces;
    iox::cxx::vector< connection_t, MAX_NET_CONNECTIONS >      connections;
};
struct thread_t
{
    uint32_t                tid{ 0u };
    el3D::bb::float_t< 64 > cpuLoad{ 0.0 };
    uint64_t                memoryUsage{ 0u };
};
struct process_t
{
    uint32_t                                              pid{ 0u };
    uint32_t                                              ppid{ 0u };
    uint32_t                                              numberOfThreads{ 0u };
    iox::cxx::vector< thread_t, MAX_THREADS_PER_PROCESS > threads;
    string                                                user;
    string                                                group;
    string                                                command;
    el3D::bb::float_t< 64 >                               cpuLoad{ 0.0 };
    uint64_t                                              memoryUsage{ 0u };
};
struct processSummary_t
{
    iox::cxx::vector< process_t, MAX_PROCESSES > processList;
    uint32_t                                     numberOfProcesses;
};

} // namespace internal

struct systemInfo_t
{
    internal::cpu_t            cpu;
    internal::memory_t         memory;
    internal::network_t        network;
    internal::processSummary_t process;

    el3D::bb::float_t< 64 > uptime{ 0.0 };
    string                  hostname;
    string                  kernelVersion;
};
} // namespace serviceData

#pragma once

#include "units/Angle.hpp"
#include "units/Length.hpp"

namespace serviceData
{
struct ultraSonicSensor_t
{
    el3D::units::Angle  angle;
    el3D::units::Length distance;
};

struct ultraSonicSensorCommand_t
{
    bool     doPerformSensorSweep{ false };
    uint16_t setAngle{ 90 };
};

} // namespace serviceData

#pragma once

#include "buildingBlocks/float_t.hpp"
#include "serviceData/buildingBlocks.hpp"

#include <array>

namespace serviceData
{
struct stereoCamera_t
{
    static constexpr int POINT_CLOUD_SIZE = 170666;
    std::array< vector_t< el3D::bb::float_t< 32 > >, POINT_CLOUD_SIZE >
                            pointCloud;
    el3D::bb::float_t< 32 > distanceToCenter;
};
} // namespace serviceData

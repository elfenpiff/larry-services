#pragma once

namespace serviceData
{
struct trackingSensor_t
{
    bool left;
    bool centerLeft;
    bool centerRight;
    bool right;
};
} // namespace serviceData

#pragma once

#include "buildingBlocks/float_t.hpp"

namespace serviceData
{
struct cameraCommand_t
{
    uint16_t xAngle{ 0u };
    uint16_t yAngle{ 0u };
};

template < uint64_t Capacity = 1024 * 1024 - 128 >
struct camera_t
{
    static constexpr uint64_t CAPACITY = Capacity;

    uint16_t        width{ 0u };
    uint16_t        height{ 0u };
    uint64_t        dataSize{ 0u };
    uint8_t         data[Capacity];
    cameraCommand_t angle;
};

} // namespace serviceData

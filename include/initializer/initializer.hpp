#pragma once

#include "iceoryx_hoofs/cxx/string.hpp"
#include "iceoryx_posh/popo/publisher.hpp"
#include "iceoryx_posh/popo/subscriber.hpp"
#include "iceoryx_posh/runtime/posh_runtime.hpp"
#include "logging/elLog.hpp"
#include "logging/policies/format.hpp"
#include "logging/policies/threading.hpp"
#include "lua/elConfigHandler.hpp"
#include "network/elEasyTCPMessaging_Server.hpp"

#include <initializer_list>
#include <memory>
#include <string>

namespace initializer
{
static constexpr char CONFIG_DIR[]    = "config";
static constexpr char ENTRY_SEPARATOR = '_';
struct iceOryxConfig_t
{
    std::string domain;
    std::string entityName;
    std::string eventCommand;
    std::string eventInfo;
    std::string eventSimulate;
    std::string eventAnnounce;
};

struct serviceConfig_t
{
    uint32_t        version;
    uint16_t        port;
    std::string     logfile;
    std::string     logDirectory;
    iceOryxConfig_t iceoryx;
};

template < typename InfoType, typename CommandType >
struct service_t
{
    service_t(
        const iox::capro::ServiceDescription& publisherService,
        const iox::capro::ServiceDescription& subscriberService,
        const iox::capro::ServiceDescription& simulatedDataService ) noexcept
        : publisher{ publisherService, iox::popo::PublisherOptions{ 0U } },
          subscriber{ subscriberService,
                      iox::popo::SubscriberOptions{ 1U, 0U } },
          simulatedData{ simulatedDataService,
                         iox::popo::SubscriberOptions{ 1U, 0U } }
    {
        publisher.offer();
        subscriber.subscribe();
        simulatedData.subscribe();
    }

    iox::popo::Publisher< InfoType >     publisher;
    iox::popo::Subscriber< CommandType > subscriber;
    iox::popo::Subscriber< InfoType >    simulatedData;
};

enum class ServiceType
{
    Command,
    Info,
    Simulate,
    Announce
};

void
InitializeLoggingToConsole();
void
InitializeLoggingToLogfile( const std::string& logfile );

serviceConfig_t
GetServiceConfig( const std::string& serviceName );
iceOryxConfig_t
GetIceOryxConfig();

std::unique_ptr< el3D::network::elEasyTCPMessaging_Server >
InitService( const std::string& serviceName );

serviceConfig_t
InitGenericService( const std::string& serviceName );

template < typename T, typename... Targs >
T
CreatePort( const std::string& instance, const ServiceType& type,
            const iceOryxConfig_t& config, const Targs&... args )
{
    using namespace iox::capro;
    using namespace iox::cxx;

    std::string eventName, serviceName = config.domain;
    if ( type == ServiceType::Announce )
    {
        eventName = config.eventAnnounce;
        serviceName += ENTRY_SEPARATOR + config.eventAnnounce;
    }
    else if ( type == ServiceType::Command )
    {
        eventName = config.entityName + ENTRY_SEPARATOR + config.eventCommand;
    }
    else if ( type == ServiceType::Info )
    {
        eventName = config.entityName + ENTRY_SEPARATOR + config.eventInfo;
    }
    else if ( type == ServiceType::Simulate )
    {
        eventName = config.entityName + ENTRY_SEPARATOR + config.eventSimulate;
    }

    return T( ServiceDescription{ IdString_t( TruncateToCapacity, serviceName ),
                                  IdString_t( TruncateToCapacity, instance ),
                                  IdString_t( TruncateToCapacity, eventName ) },
              args... );
}

iox::capro::ServiceDescription
CreateServiceDescription( const std::string& instance, const ServiceType& type,
                          const iceOryxConfig_t& config );

template < typename InfoType, typename CommandType >
service_t< InfoType, CommandType >
CreateService( const std::string& serviceName )
{
    auto service = InitGenericService( serviceName );
    iox::runtime::PoshRuntime::initRuntime(
        iox::NodeName_t( iox::cxx::TruncateToCapacity, serviceName ) );
    LOG_INFO( 0 ) << "starting Ice0ryx service : " << serviceName;

    return service_t< InfoType, CommandType >{
        CreateServiceDescription( serviceName, ServiceType::Info,
                                  service.iceoryx ),
        CreateServiceDescription( serviceName, ServiceType::Command,
                                  service.iceoryx ),
        CreateServiceDescription( serviceName, ServiceType::Simulate,
                                  service.iceoryx ) };
}

template < typename InfoType >
service_t< InfoType, InfoType >
CreateService( const std::string& serviceName )
{
    return CreateService< InfoType, InfoType >( serviceName );
}

el3D::lua::elConfigHandler&
GetConfig();

template < typename T >
std::vector< T >
GetConfigEntry( const std::function< bool( std::vector< T >& ) >& isValid,
                const std::initializer_list< std::string >&       entry,
                const std::string&                                errorHint )
{
    auto entryValue = GetConfig().Get< T >( entry );
    if ( !isValid( entryValue ) )
    {
        std::string entryPath = "";
        for ( auto v : entry )
            entryPath += v + ".";
        entryPath.resize( entryPath.size() - 1 );

        LOG_FATAL( 0 ) << errorHint << " Please adjust the entry \""
                       << entryPath << "\" in config found in \""
                       << initializer::CONFIG_DIR << "\"";
        exit( 1 );
    }
    return entryValue;
}

} // namespace initializer

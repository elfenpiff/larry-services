#pragma once

#include "gpio/GPIOWriter.hpp"

namespace driver
{
class MotorController
{
  public:
    MotorController( const int gpioPin, const int angleBias = 0 ) noexcept;

    void
    Up() noexcept;

    void
    Down() noexcept;

    uint16_t
    GetCurrentAngle() const noexcept;

    void
    SetAngle( const uint16_t angle ) noexcept;

    void
    SetSpeed( const uint16_t speed ) noexcept;

  private:
    void
    AdjustAngle( const uint16_t angle ) noexcept;

  private:
    gpio::GPIOWriter motor;
    int              angleBias{ 0 };
    uint16_t         currentAngle{ 90u };
    uint16_t         angleSpeed{ 1u };
};
} // namespace driver

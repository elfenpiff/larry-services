#pragma once

#ifdef ENABLE_GPIO
#include <wiringPi.h>
#endif

namespace gpio
{
void
init() noexcept;
}

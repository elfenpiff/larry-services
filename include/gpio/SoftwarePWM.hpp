#pragma once

#ifdef ENABLE_GPIO
#include <softPwm.h>
#include <wiringPi.h>
#endif

namespace gpio
{
class SoftwarePWM
{
  public:
    SoftwarePWM( const int pin, const int initialValue,
                 const int range ) noexcept;
    void
    Write( const int value ) noexcept;

  private:
#ifdef ENABLE_GPIO
    int pin;
#endif
};
} // namespace gpio

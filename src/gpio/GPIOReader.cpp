#include "gpio/GPIOReader.hpp"

#include "gpio/init.hpp"

namespace gpio
{
#ifdef ENABLE_GPIO
GPIOReader::GPIOReader( const int pin ) noexcept : pin( pin )
{
    init();
    pinMode( this->pin, INPUT );
}
#else
GPIOReader::GPIOReader( const int ) noexcept
{
    init();
}
#endif


bool
GPIOReader::IsHigh() const noexcept
{
#ifdef ENABLE_GPIO
    return 1 == digitalRead( this->pin );
#endif
    return true;
}
} // namespace gpio

#include "gpio/SoftwarePWM.hpp"

#include "gpio/init.hpp"

namespace gpio
{
#ifdef ENABLE_GPIO
SoftwarePWM::SoftwarePWM( const int pin, const int initialValue,
                          const int range ) noexcept
    : pin{ pin }
{
    init();
    softPwmCreate( this->pin, initialValue, range );
}
#else
SoftwarePWM::SoftwarePWM( const int, const int, const int ) noexcept
{
    init();
}
#endif

void
SoftwarePWM::Write( const int value ) noexcept
{
#ifdef ENABLE_GPIO
    softPwmWrite( this->pin, value );
#else
    (void)value;
#endif
}


} // namespace gpio


#include "gpio/GPIOWriter.hpp"

#include "gpio/init.hpp"

#include <chrono>
#include <cstdint>
#include <thread>

namespace gpio
{
#ifdef ENABLE_GPIO
GPIOWriter::GPIOWriter( const int pin ) noexcept : pin{ pin }
{
    init();
    pinMode( this->pin, OUTPUT );
}
#else
GPIOWriter::GPIOWriter( const int ) noexcept
{
    init();
}
#endif

void
GPIOWriter::High( const el3D::units::Time delay ) noexcept
{
    this->Write( 1, delay );
}

void
GPIOWriter::Low( const el3D::units::Time delay ) noexcept
{
    this->Write( 0, delay );
}

bool
GPIOWriter::IsHigh() const noexcept
{
    return this->isHigh;
}

void
GPIOWriter::Write( const int value, const el3D::units::Time delay ) noexcept
{
#ifdef ENABLE_GPIO
    digitalWrite( this->pin, value );
    delayMicroseconds( static_cast< unsigned int >( delay.GetMicroSeconds() ) );
#else
    (void)delay;
#endif
    this->isHigh = !( value == 0 );
}


} // namespace gpio

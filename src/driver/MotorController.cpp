#include "driver/MotorController.hpp"

#include <algorithm>

namespace driver
{
MotorController::MotorController( const int gpioPin,
                                  const int angleBias ) noexcept
    : motor( gpioPin ), angleBias( angleBias )
{
    this->SetAngle( 90 );
}

void
MotorController::Up() noexcept
{
    this->AdjustAngle( this->GetCurrentAngle() + this->angleSpeed );
}

void
MotorController::Down() noexcept
{
    this->AdjustAngle( ( this->angleSpeed <= this->GetCurrentAngle() )
                           ? this->GetCurrentAngle() - this->angleSpeed
                           : 0u );
}

uint16_t
MotorController::GetCurrentAngle() const noexcept
{
    return this->currentAngle;
}

void
MotorController::SetAngle( const uint16_t v ) noexcept
{
    for ( int i = 0; i < 10; ++i )
        this->AdjustAngle( v );
}

void
MotorController::AdjustAngle( const uint16_t v ) noexcept
{
    using namespace el3D::units;
    this->currentAngle = std::clamp( v, static_cast< uint16_t >( 0u ),
                                     static_cast< uint16_t >( 180u ) );
    int pulseWidth = 500 + ( ( this->angleBias + this->currentAngle ) * 11 );

    this->motor.High( Time::MicroSeconds( pulseWidth ) );
    this->motor.Low( Time::MilliSeconds( 20 - pulseWidth / 1000 ) );
}

void
MotorController::SetSpeed( const uint16_t speed ) noexcept
{
    this->angleSpeed = speed;
}

} // namespace driver

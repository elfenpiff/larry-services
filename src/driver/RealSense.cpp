#ifdef ENABLE_LIBREALSENSE

#include "driver/RealSense.hpp"

#include "logging/elLog.hpp"

namespace driver
{
std::optional< RealSense::stereoCamera_t >
RealSense::AcquireStereoCamera( const std::string& sensorName )
{
    device_t device( sensorName, *this->context );
    return ( device.isInitialized )
               ? std::make_optional( stereoCamera_t{ std::move( device ) } )
               : std::nullopt;
}

std::optional< RealSense::trackingCamera_t >
RealSense::AcquireTrackingCamera( const std::string& sensorName )
{
    device_t device( sensorName, *this->context );
    return ( device.isInitialized )
               ? std::make_optional( trackingCamera_t{ std::move( device ) } )
               : std::nullopt;
}

RealSense::device_t::device_t( const std::string&  sensorName,
                               const rs2::context& context ) noexcept
{
    for ( auto&& device : context.query_devices() )
        if ( std::string( device.get_info( RS2_CAMERA_INFO_NAME ) ) ==
             sensorName )
        {
            LOG_INFO( 0 ) << "found realsense device: " << sensorName;
            this->pipeline = context;
            this->config.enable_device(
                device.get_info( RS2_CAMERA_INFO_SERIAL_NUMBER ) );
            this->isInitialized = true;
            return;
        }

    LOG_WARN( 0 ) << "unable to find realsense device: " << sensorName;
}

RealSense::device_t::~device_t()
{
    this->destroy();
}

void
RealSense::device_t::destroy() noexcept
{
    if ( this->isInitialized )
    {
        this->pipeline.stop();
        this->config.disable_all_streams();
        this->isInitialized = false;
    }
}

RealSense::device_t::device_t( device_t&& rhs ) noexcept
{
    *this = std::move( rhs );
}

RealSense::device_t&
RealSense::device_t::operator=( device_t&& rhs ) noexcept
{
    if ( this != &rhs )
    {
        this->destroy();

        this->pipeline    = std::move( rhs.pipeline );
        this->config      = std::move( rhs.config );
        rhs.isInitialized = false;
    }
    return *this;
}

void
RealSense::stereoCamera_t::StartStreaming()
{
    this->device.pipeline.start( this->device.config );
}

bool
RealSense::stereoCamera_t::GetOutput( serviceData::stereoCamera_t& newData )
{
    rs2::points      points;
    rs2::pointcloud  pointCloud;
    rs2::frameset    data       = this->device.pipeline.wait_for_frames();
    rs2::depth_frame depthFrame = data.get_depth_frame();
    if ( !depthFrame )
    {
        LOG_ERROR( 0 ) << "error with depth frame";
        return false;
    }

    this->filter.process( depthFrame );
    points = pointCloud.calculate( depthFrame );

    auto vertices            = points.get_vertices();
    newData.distanceToCenter = depthFrame.get_distance(
        depthFrame.get_width() / 2, depthFrame.get_height() / 2 );
    int pointCloudSize = points.get_data_size() /
                         ( 3 * static_cast< int >( sizeof( *vertices ) ) );

    if ( pointCloudSize > serviceData::stereoCamera_t::POINT_CLOUD_SIZE )
    {
        LOG_WARN( 0 )
            << "PointCloud has " << pointCloudSize
            << " points and is larger then the maximum supported size of "
            << serviceData::stereoCamera_t::POINT_CLOUD_SIZE
            << "points, therefore we will loose some points!";
    }

    for ( int i = 0; i < pointCloudSize &&
                     i < serviceData::stereoCamera_t::POINT_CLOUD_SIZE;
          ++i )
    {
        newData.pointCloud[static_cast< size_t >( i )] =
            serviceData::vector_t< el3D::bb::float_t< 32 > >{
                vertices[i].x, vertices[i].y, vertices[i].z };
    }

    return true;
}

RealSense::stereoCamera_t::filter_t::filter_t()
{
    this->decimation.set_option( RS2_OPTION_FILTER_MAGNITUDE, 8 );
    this->holeFilling.set_option( RS2_OPTION_HOLES_FILL, 1 );
}

void
RealSense::stereoCamera_t::filter_t::process( rs2::frame& frame ) const
{
    this->decimation.process( frame );
    this->depthToDisparity.process( frame );
    this->temporal.process( frame );
    this->holeFilling.process( frame );
}

void
RealSense::trackingCamera_t::StartStreaming()
{
    this->device.pipeline.start( this->device.config );
}

bool
RealSense::trackingCamera_t::GetOutput( serviceData::trackingCamera_t& newData )
{
    using namespace el3D;
    auto frame = this->device.pipeline.wait_for_frames().first_or_default(
        RS2_STREAM_POSE );
    auto v = frame.as< rs2::pose_frame >().get_pose_data();

    newData.acceleration.x =
        units::Acceleration::MeterPerSecondSquare( v.acceleration.x );
    newData.acceleration.y =
        units::Acceleration::MeterPerSecondSquare( v.acceleration.y );
    newData.acceleration.z =
        units::Acceleration::MeterPerSecondSquare( v.acceleration.z );

    newData.angularAcceleration.x =
        units::AngularAcceleration::RadiansPerSecondSquare(
            v.angular_acceleration.x );
    newData.angularAcceleration.y =
        units::AngularAcceleration::RadiansPerSecondSquare(
            v.angular_acceleration.y );
    newData.angularAcceleration.z =
        units::AngularAcceleration::RadiansPerSecondSquare(
            v.angular_acceleration.z );

    newData.velocity.x = units::Velocity::MeterPerSecond( v.velocity.x );
    newData.velocity.y = units::Velocity::MeterPerSecond( v.velocity.y );
    newData.velocity.z = units::Velocity::MeterPerSecond( v.velocity.z );

    newData.angularVelocity.x =
        units::AngularVelocity::RadiansPerSecond( v.angular_velocity.x );
    newData.angularVelocity.y =
        units::AngularVelocity::RadiansPerSecond( v.angular_velocity.y );
    newData.angularVelocity.z =
        units::AngularVelocity::RadiansPerSecond( v.angular_velocity.z );

    newData.translation.x = units::Length::Meter( v.translation.x );
    newData.translation.y = units::Length::Meter( v.translation.y );
    newData.translation.z = units::Length::Meter( v.translation.z );

    newData.rotation.x = v.rotation.x;
    newData.rotation.y = v.rotation.y;
    newData.rotation.z = v.rotation.z;
    newData.rotation.w = v.rotation.w;

    newData.trackerConfidence = units::Confidence( v.tracker_confidence );
    newData.mapperConfidence  = units::Confidence( v.mapper_confidence );

    return true;
}

} // namespace driver

#endif

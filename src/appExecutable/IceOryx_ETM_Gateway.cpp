#include "iceoryx_on_fire/generic_gateway.hpp"
#include "iceoryx_on_fire/typed_publisher.hpp"
#include "iceoryx_posh/capro/service_description.hpp"
#include "iceoryx_utils/cxx/string.hpp"
#include "initializer/initializer.hpp"
#include "network/etm/Transmission_t.hpp"
#include "serviceData/drive_t.hpp"
#include "serviceData/ledControl_t.hpp"
#include "serviceData/motorControl_t.hpp"
#include "serviceData/trackingSensor_t.hpp"
#include "serviceData/ultraSonicSensor_t.hpp"
#include "utils/memcpy_pp.hpp"

#include <map>
#include <thread>

using server_t = std::unique_ptr< el3D::network::elEasyTCPMessaging_Server >;
namespace gateway
{
std::map< std::string, server_t > services;

bool
Connector( const iox::capro::ServiceDescription& service )
{
    static initializer::iceOryxConfig_t iceConfig =
        initializer::GetIceOryxConfig();
    if ( service.getServiceIDString() == iceConfig.entityName &&
         service.getEventIDString() == iceConfig.eventInfo )
    {
        auto serviceConfig =
            initializer::GetServiceConfig( service.getInstanceIDString() );

        server_t server( new el3D::network::elEasyTCPMessaging_Server(
            service.getInstanceIDString(), serviceConfig.version ) );
        if ( server->Listen( serviceConfig.port ).HasError() ) return false;

        services[service.getInstanceIDString()] = std::move( server );

        std::cout << "service : " << service.getServiceIDString() << "."
                  << service.getInstanceIDString() << "."
                  << service.getEventIDString() << " is listening on "
                  << serviceConfig.port << std::endl;
        return true;
    }
    return false;
}

void
Disconnector( const iox::capro::ServiceDescription& service )
{
    auto iter = services.find( service.getInstanceIDString() );
    if ( iter == services.end() ) return;

    iter->second->StopListen();
    services.erase( iter );

    std::cout << "service stopped : " << service.getServiceIDString() << "."
              << service.getInstanceIDString() << "."
              << service.getEventIDString() << std::endl;
}

void
Transmitter( const iox::capro::ServiceDescription& service,
             void* const                           payload )
{
    using namespace el3D::network::etm;
    auto iter = services.find( service.getInstanceIDString() );
    if ( iter == services.end() ) return;

    if ( service.getInstanceIDString() ==
         iox::capro::IdString( "TrackingSensor" ) )
    {
        auto data =
            reinterpret_cast< serviceData::trackingSensor_t* >( payload );
        iter->second->SendBroadcastMessage< serviceData::trackingSensor_t >(
            0, MessageType::Stream, *data );
    }
    else if ( service.getInstanceIDString() ==
              iox::capro::IdString( "UltraSonicSensor" ) )
    {
        auto data =
            reinterpret_cast< serviceData::ultraSonicSensor_t* >( payload );
        iter->second->SendBroadcastMessage< serviceData::ultraSonicSensor_t >(
            0, MessageType::Stream, *data );
    }
    else // binary connections
    {
        auto dataSize = *reinterpret_cast< uint64_t* >( payload );
        el3D::utils::byteStream_t byteStream;
        byteStream.stream.resize( dataSize );

        el3D::utils::MemCpy::From(
            static_cast< void* >( static_cast< uint64_t* >( payload ) + 1 ) )
            .To( static_cast< void* >( byteStream.stream.data() ) )
            .Size( dataSize );
        iter->second->SendBroadcastMessage< el3D::utils::byteStream_t >(
            0, MessageType::Stream, byteStream );
    }
}
} // namespace gateway

std::vector< std::thread > activeServices;
std::atomic_bool           keepRunning{ true };

template < typename TopicType >
void
AddExternalService( const std::string& serviceName )
{
    using namespace el3D::utils;
    using namespace el3D::network::etm;
    auto serviceConfig = initializer::GetServiceConfig( serviceName );

    activeServices.emplace_back( [serviceName, serviceConfig] {
        server_t server( new el3D::network::elEasyTCPMessaging_Server(
            serviceName, serviceConfig.version ) );
        if ( server->Listen( serviceConfig.port ).HasError() ) return;

        std::cout << "service : " << serviceConfig.iceoryx.entityName << "."
                  << serviceName << "." << serviceConfig.iceoryx.eventCommand
                  << " is listening on " << serviceConfig.port << std::endl;

        iox::TypedPublisher< TopicType > publisher(
            { iox::capro::IdString( iox::cxx::TruncateToCapacity,
                                    serviceConfig.iceoryx.entityName ),
              iox::capro::IdString( iox::cxx::TruncateToCapacity, serviceName ),
              iox::capro::IdString( iox::cxx::TruncateToCapacity,
                                    serviceConfig.iceoryx.eventCommand ) } );

        while ( keepRunning.load() )
        {
            auto message = server->BlockingReceiveMessages();
            if ( !message.empty() )
            {
                Transmission_t< TopicType > data;
                data.Deserialize( Endian::Big, message.back().message, 0 )
                    .OnSuccess( [&] {
                        publisher.allocateAndPublish( data.payload.payload );
                    } );
            }
        }
    } );
}

int
main()
{
    initializer::InitGenericService( "ServiceInfoProvider" );
    auto& runtime =
        iox::runtime::PoshRuntime::getInstance( "/iceoryx_gateway" );

    AddExternalService< serviceData::drive_t >( "Drive" );
    AddExternalService< serviceData::ledControl_t >( "LedControl" );
    AddExternalService< serviceData::ultraSonicSensorCommand_t >(
        "UltraSonicSensorCommand" );
    AddExternalService< serviceData::motorControl_t >( "CameraVerticalMotor" );
    AddExternalService< serviceData::motorControl_t >(
        "CameraHorizontalMotor" );

    iox::GenericGateway ice2etm( &runtime, &gateway::Connector,
                                 &gateway::Disconnector,
                                 &gateway::Transmitter );


    std::this_thread::sleep_for( std::chrono::seconds( 100000 ) );

    return ( EXIT_SUCCESS );
}

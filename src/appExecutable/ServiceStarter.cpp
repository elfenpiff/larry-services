#include "cui/CommandState.hpp"
#include "cui/Cui.hpp"
#include "cui/Label.hpp"
#include "cui/ProgressBar.hpp"
#include "cuipron/ConsoleControl.hpp"
#include "initializer/initializer.hpp"

#include <algorithm>

constexpr char BIN_PREFIX[] = "./build/";

auto mgmtApps =
    initializer::GetConfig().Get< std::string >( { "management", "active" } );
auto services =
    initializer::GetConfig().Get< std::string >( { "services", "active" } );

auto apps = [] {
    std::vector< std::string > v1;
    v1.insert( v1.end(), mgmtApps.begin(), mgmtApps.end() );
    v1.insert( v1.end(), services.begin(), services.end() );
    return v1;
}();
uint64_t                                            numberOfApps = apps.size();
uint64_t                                            activatedApps{ 0 };
std::string                                         activeApp;
std::vector< std::shared_ptr< cui::CommandState > > appCommands;
std::vector< std::shared_ptr< cui::Label > >        errorLabels;

void
activateNextApp()
{
    if ( appCommands.size() > activatedApps )
        appCommands[activatedApps]->Execute();
    activeApp = apps[activatedApps++];
}

int
main()
{
    uint16_t lineSpacing = static_cast< uint16_t >( 2 * numberOfApps + 1 );

    cui::Cui cui( static_cast< uint16_t >( lineSpacing ) );
    auto     cursorPos = cui.GetStartupCursorPosition();
    cursorPos.y        = std::max( cursorPos.y, lineSpacing );
    auto terminalWidth = cp::ConsoleControl().GetTerminalDimensions().x;

    auto progressBar = cui.Create< cui::ProgressBar >(
        { .size     = { terminalWidth, 1 },
          .position = {
              1, static_cast< uint16_t >( cursorPos.y - lineSpacing ) } } );

    progressBar->SetProgress(
        [&] { return activatedApps * 100 / numberOfApps; } );
    progressBar->SetDescription( [&] { return activeApp; } );

    size_t p = 0;
    for ( size_t k = 0, limit = mgmtApps.size(); k < limit; ++k, ++p )
    {
        auto command = initializer::GetConfig().Get< std::string >(
            { "management", mgmtApps[k], "path" } );

        if ( command.empty() )
        {
            errorLabels.emplace_back( cui.Create< cui::Label >(
                { .size     = { terminalWidth, 1 },
                  .position = { 1,
                                static_cast< uint16_t >(
                                    cursorPos.y - lineSpacing + 1 + p ) } } ) );
            errorLabels.back()->SetText( [=]() -> std::string {
                return "unable to find " + mgmtApps[k] + " path entry";
            } );
            continue;
        }

        appCommands.emplace_back( cui.Create< cui::CommandState >(
            { .size     = { terminalWidth, 1 },
              .position = { 1, static_cast< uint16_t >(
                                   cursorPos.y - lineSpacing + 1 + p ) } },
            command[0] ) );
    }
    for ( size_t k = 0, limit = services.size(); k < limit; ++k, ++p )
    {
        auto command = initializer::GetConfig().Get< std::string >(
            { "services", services[k], "command" } )[0];

        appCommands.emplace_back( cui.Create< cui::CommandState >(
            { .size     = { terminalWidth, 1 },
              .position = { 1, static_cast< uint16_t >(
                                   cursorPos.y - lineSpacing + 1 + p ) } },
            std::string( BIN_PREFIX ) + command ) );
    }

    while ( numberOfApps != activatedApps )
    {
        std::this_thread::sleep_for( std::chrono::milliseconds( 250 ) );
        activateNextApp();
    }

    cui.WaitForExitSignal();
}

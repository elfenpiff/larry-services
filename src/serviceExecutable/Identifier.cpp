#include "iceoryx_hoofs/cxx/string.hpp"
#include "iceoryx_posh/iceoryx_posh_types.hpp"
#include "initializer/initializer.hpp"
#include "serviceData/identifier_t.hpp"

using namespace el3D;

constexpr char SERVICE[] = "Identifier";

int
main()
{
    auto config = initializer::InitGenericService( SERVICE );
    iox::runtime::PoshRuntime::initRuntime( iox::NodeName_t(
        iox::cxx::TruncateToCapacity, std::string( SERVICE ) ) );

    serviceData::identifier_t identifier;
    identifier.name = serviceData::identifier_t::string(
        iox::cxx::TruncateToCapacity, config.iceoryx.entityName.c_str() );

    auto availableServices =
        initializer::GetConfig().Get< std::string >( { "services", "active" } );
    for ( auto& s : availableServices )
        identifier.availableServices.emplace_back(
            serviceData::identifier_t::string( iox::cxx::TruncateToCapacity,
                                               s ) );
    auto domain = initializer::GetConfig().Get< std::string >(
        { "Robot", "domain" }, { "larry.robotics" } )[0];

    auto sender = initializer::CreatePort<
        iox::popo::Publisher< serviceData::identifier_t > >(
        config.iceoryx.entityName, initializer::ServiceType::Announce,
        config.iceoryx, iox::popo::PublisherOptions{ 0U } );
    sender.offer();
    sender.publishCopyOf( identifier );

    LOG_INFO( 0 ) << "announcing \"" << identifier.name << "\" with "
                  << identifier.availableServices.size() << " services";

    while ( true )
    {
        sender.publishCopyOf( identifier );
        std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
    }
}

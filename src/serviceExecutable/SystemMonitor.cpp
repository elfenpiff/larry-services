#include "initializer/initializer.hpp"
#include "network/elEasyTCPMessaging_Server.hpp"
#include "network/etm/Transmission_t.hpp"
#include "network/etm/etm.hpp"
#include "serviceData/systemMonitor_t.hpp"
#include "utils/byteStream_t.hpp"
#include "utils/elSystemInfo.hpp"
#include "utils/systemInfoTypes.hpp"

#include <chrono>
#include <iostream>

using namespace el3D;

constexpr char SERVICE[] = "SystemMonitor";

serviceData::string
convert( const std::string& s )
{
    return { iox::cxx::TruncateToCapacity, s };
}

serviceData::internal::cpuCore_t
convert( const el3D::utils::internal::cpuCore_t& cpuCore )
{
    return { cpuCore.frequencyInMhz, cpuCore.load };
}

serviceData::internal::cpu_t
convert( const el3D::utils::internal::cpu_t& cpu )
{
    serviceData::internal::cpu_t value;
    value.model         = convert( cpu.model );
    value.load          = cpu.load;
    value.numberOfCores = cpu.numberOfCores;

    for ( uint64_t i     = 0,
                   limit = std::min( cpu.cores.size(), value.cores.capacity() );
          i < limit; ++i )
        value.cores.emplace_back( convert( cpu.cores[i] ) );
    return value;
}

serviceData::internal::memory_t
convert( const el3D::utils::internal::memory_t& memory )
{
    return { memory.total,  memory.used,      memory.cached,
             memory.buffer, memory.swapTotal, memory.swapUsed };
}

serviceData::internal::netStat_t
convert( const el3D::utils::internal::netStat_t& netStat )
{
    return { netStat.bytes, netStat.packets, netStat.byteLoad,
             netStat.packetLoad, netStat.errors };
}

serviceData::internal::networkInterface_t
convert( const el3D::utils::internal::networkInterface_t& interface )
{
    return { convert( interface.incoming ),
             convert( interface.outgoing ),
             convert( interface.interfaceName ),
             convert( interface.macAddress ),
             convert( interface.ipAddress ),
             convert( interface.broadCastAddress ),
             convert( interface.networkMask ) };
}

serviceData::internal::connectionPart_t
convert( const el3D::utils::internal::connectionPart_t& c )
{
    return { convert( c.ipAddress ), c.port };
}

serviceData::internal::connection_t
convert( const el3D::utils::internal::connection_t& c )
{
    return { convert( c.source ), convert( c.destination ) };
}

serviceData::internal::network_t
convert( const el3D::utils::internal::network_t& n )
{
    serviceData::internal::network_t value;

    for ( uint64_t i     = 0,
                   limit = std::min( n.dns.size(), value.dns.capacity() );
          i < limit; ++i )
        value.dns.emplace_back( convert( n.dns[i] ) );

    for ( uint64_t i = 0, limit = std::min( n.interfaces.size(),
                                            value.interfaces.capacity() );
          i < limit; ++i )
        value.interfaces.emplace_back( convert( n.interfaces[i] ) );

    for ( uint64_t i = 0, limit = std::min( n.connections.size(),
                                            value.connections.capacity() );
          i < limit; ++i )
        value.connections.emplace_back( convert( n.connections[i] ) );

    return value;
}

serviceData::internal::thread_t
convert( const el3D::utils::internal::thread_t& t )
{
    return { t.tid, t.cpuLoad, t.memoryUsage };
}

serviceData::internal::process_t
convert( const el3D::utils::internal::process_t& p )
{
    serviceData::internal::process_t value;
    value.pid             = p.pid;
    value.ppid            = p.ppid;
    value.numberOfThreads = p.numberOfThreads;
    value.user            = convert( p.user );
    value.group           = convert( p.group );
    value.command         = convert( p.command );
    value.cpuLoad         = p.cpuLoad;
    value.memoryUsage     = p.memoryUsage;

    for ( uint64_t i = 0, limit = std::min( p.threads.size(),
                                            value.threads.capacity() );
          i < limit; ++i )
        value.threads.emplace_back( convert( p.threads[i] ) );

    return value;
}

serviceData::internal::processSummary_t
convert( const el3D::utils::internal::processSummary_t& p )
{
    serviceData::internal::processSummary_t value;
    value.numberOfProcesses = p.numberOfProcesses;
    for ( uint64_t i = 0, limit = std::min( p.processList.size(),
                                            value.processList.capacity() );
          i < limit; ++i )
        value.processList.emplace_back( convert( p.processList[i] ) );

    return value;
}

serviceData::systemInfo_t
convert( const el3D::utils::systemInfo_t& s )
{
    return { convert( s.cpu ),
             convert( s.memory ),
             convert( s.network ),
             convert( s.process ),
             s.uptime,
             convert( s.hostname ),
             convert( s.kernelVersion ) };
}

int
main()
{
    auto service =
        initializer::CreateService< serviceData::systemInfo_t >( SERVICE );

    utils::elSystemInfo sysInfo( units::Time::MilliSeconds( 1000.0 ) );

    for ( uint64_t transmissionID = 0; true; ++transmissionID )
    {
        service.publisher.publishCopyOf( convert( sysInfo.GetSystemInfo() ) );
        std::this_thread::sleep_for( std::chrono::milliseconds( 1000 ) );
    }
}

#include "gpio/GPIOReader.hpp"
#include "initializer/initializer.hpp"
#include "network/etm/etm.hpp"
#include "serviceData/trackingSensor_t.hpp"
#include "units/Time.hpp"

#include <array>
#include <chrono>

using namespace el3D;

constexpr char    SERVICE[]            = "TrackingSensor";
constexpr int     PINS_LEFT_TO_RIGHT[] = { 9, 21, 7, 1 };
const units::Time inLoopWait           = units::Time::MilliSeconds( 25.0 );

class TrackingSensor
{
  public:
    TrackingSensor( const std::array< int, 4 >& pinsLeftToRight ) noexcept;
    serviceData::trackingSensor_t
    Get() const noexcept;

  private:
    gpio::GPIOReader left;
    gpio::GPIOReader centerLeft;
    gpio::GPIOReader centerRight;
    gpio::GPIOReader right;
};

TrackingSensor::TrackingSensor(
    const std::array< int, 4 >& pinsLeftToRight ) noexcept
    : left( pinsLeftToRight[0] ), centerLeft( pinsLeftToRight[1] ),
      centerRight( pinsLeftToRight[2] ), right( pinsLeftToRight[3] )
{
}

serviceData::trackingSensor_t
TrackingSensor::Get() const noexcept
{
    serviceData::trackingSensor_t sensor;
    sensor.left        = left.IsHigh();
    sensor.centerLeft  = centerLeft.IsHigh();
    sensor.centerRight = centerRight.IsHigh();
    sensor.right       = right.IsHigh();
    return sensor;
}

int
main()
{
    auto service =
        initializer::CreateService< serviceData::trackingSensor_t >( SERVICE );

    TrackingSensor tracker( { PINS_LEFT_TO_RIGHT[0], PINS_LEFT_TO_RIGHT[1],
                              PINS_LEFT_TO_RIGHT[2], PINS_LEFT_TO_RIGHT[3] } );

    for ( uint64_t transmissionID = 0; true; ++transmissionID )
    {
        service.publisher.publishCopyOf( tracker.Get() );

        std::this_thread::sleep_for( std::chrono::nanoseconds(
            static_cast< uint64_t >( inLoopWait.GetNanoSeconds() ) ) );
    }
}

#include "concurrent/elSmartLock.hpp"
#include "gpio/GPIOWriter.hpp"
#include "gpio/SoftwarePWM.hpp"
#include "initializer/initializer.hpp"
#include "logging/elLog.hpp"
#include "network/etm/Transmission_t.hpp"
#include "serviceData/drive_t.hpp"
#include "units/Time.hpp"

#include <cstdint>
#include <glm/fwd.hpp>
#include <ios>
#include <thread>

using namespace el3D;

constexpr char SERVICE[] = "Drive";

struct motorPins_t
{
    int forward;
    int backward;
    int speed;
};

constexpr motorPins_t LEFT_PINS{ 28, 29, 27 };
constexpr motorPins_t RIGHT_PINS{ 24, 25, 23 };
constexpr int         MOTOR_RESET_IN_MS{ 100 };

class Motor
{
  public:
    Motor( const motorPins_t& pins )
        : forward( pins.forward ), backward( pins.backward ),
          speed( pins.speed, 0, 100 )
    {
    }

    void
    Forward( const int speedInPercent )
    {
        this->forward.High();
        this->backward.Low();
        this->speed.Write( std::clamp( speedInPercent, -100, 100 ) );
    }

    void
    Backward( const int speedInPercent )
    {
        this->forward.Low();
        this->backward.High();
        this->speed.Write( std::clamp( speedInPercent, -100, 100 ) );
    }

    void
    Move( const int directionalSpeedInPercent )
    {
        if ( directionalSpeedInPercent > 0 )
            this->Forward( directionalSpeedInPercent );
        else if ( directionalSpeedInPercent < 0 )
            this->Backward( abs( directionalSpeedInPercent ) );
        else if ( directionalSpeedInPercent == 0 )
            this->Brake();
    }

    void
    Brake()
    {
        this->forward.Low();
        this->backward.Low();
    }

  private:
    gpio::GPIOWriter  forward;
    gpio::GPIOWriter  backward;
    gpio::SoftwarePWM speed;
};

using motor_t = concurrent::elSmartLock< Motor >;

motor_t leftMotor  = motor_t::Create( LEFT_PINS );
motor_t rightMotor = motor_t::Create( RIGHT_PINS );

int
main()
{
    auto service =
        initializer::CreateService< serviceData::drive_t >( SERVICE );

    service.publisher.publishCopyOf( serviceData::drive_t() );

    std::atomic_bool        keepRunning{ true };
    std::condition_variable cv;
    std::mutex              cvMutex;
    serviceData::drive_t    lastDriveState{ 0, 0 };

    auto setMotor = [&]( serviceData::drive_t cmd ) {
        if ( cmd.left != 0 || cmd.right != 0 ) cv.notify_one();
        if ( lastDriveState.left == cmd.left &&
             lastDriveState.right == cmd.right )
            return;

        leftMotor->Move( cmd.left );
        rightMotor->Move( cmd.right );
        LOG_INFO( 0 ) << "drive command: left = "
                      << static_cast< int64_t >( cmd.left )
                      << ", right = " << static_cast< int64_t >( cmd.right );
        service.publisher.publishCopyOf( cmd );

        lastDriveState = cmd;
    };

    std::thread throttleThread( [&] {
        while ( keepRunning )
        {
            std::unique_lock< std::mutex > lk( cvMutex );
            if ( cv.wait_for(
                     lk, std::chrono::milliseconds( MOTOR_RESET_IN_MS ) ) ==
                 std::cv_status::timeout )
                setMotor( serviceData::drive_t{ 0, 0 } );
        }
    } );


    while ( true )
    {
        service.subscriber.take().and_then(
            [&]( iox::popo::Sample< const serviceData::drive_t >&
                     driveCommand ) { setMotor( *driveCommand.get() ); } );
    }

    keepRunning = false;
    throttleThread.join();
}
